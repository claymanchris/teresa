import { fromEvent, empty } from 'rxjs'
import * as THREE from 'three'
import {
  pairwise,
  mergeMap,
  scan,
  timeoutWith,
  flatMap,
  map,
  takeUntil,
  distinctUntilChanged,
  filter
} from 'rxjs/operators'
import {
  latLng,
  distance,
  camera,
  sphereGroup,
  messageGroup,
  start$
} from './subjects'

const getIntersect = (x, y) => {
  const INTERSECTION_GROUPS = [sphereGroup, messageGroup]
  const raycaster = new THREE.Raycaster()
  const mouseVector = new THREE.Vector3()
  const width = window.innerWidth
  const height = window.innerHeight

  x = (x / width) * 2 - 1
  y = -(y / height) * 2 + 1

  mouseVector.set(x, y, 0.5)
  raycaster.setFromCamera(mouseVector, camera)
  const intersects = raycaster.intersectObjects(INTERSECTION_GROUPS, true)
  if (intersects.length > 0) {
    const res = intersects.find((res) => res && res.object)
    if (res) {
      return res.object
    }
  }
  return null
}

export const down$ = fromEvent(document, 'mousedown')
export const move$ = fromEvent(document, 'mousemove')
export const up$ = fromEvent(document, 'mouseup')
export const click$ = fromEvent(document, 'click')
export const pan$ = down$.pipe(
  mergeMap((down) => {
    const [originalLat, originalLng] = latLng.value
    return move$.pipe(
      map((move) => ({ down, move, originalLng, originalLat })),
      takeUntil(up$)
    )
  })
)
export const wheel$ = fromEvent(document, 'wheel').pipe(
  map((event) => event.deltaY),
  scan(
    (d, deltaY) => THREE.MathUtils.clamp(d + deltaY * 0.05, 1, 50),
    distance.value
  )
)
export const resize$ = fromEvent(window, 'resize')
export const intersect$ = move$.pipe(
  map((e) => getIntersect(e.layerX, e.layerY)),
  distinctUntilChanged()
)

export const intersectEnter$ = intersect$.pipe(
  pairwise(),
  filter((pair) => pair[0] === null),
  map((pair) => pair[1])
)

export const intersectLeave$ = intersect$.pipe(
  pairwise(),
  filter((pair) => pair[1] === null),
  map((pair) => pair[0])
)

export const intersectClick$ = click$.pipe(
  map((e) => getIntersect(e.layerX, e.layerY)),
  filter((obj) => !!obj)
)

// mouse up and didnt pan/drag, and no intersect
export const nonIntersectClick$ = down$.pipe(
  map((e) => getIntersect(e.layerX, e.layerY)),
  filter((obj) => !obj),
  flatMap(() => up$.pipe(timeoutWith(200, empty())))
)

export const intersectMessageClick$ = intersectClick$.pipe(
  filter((object) => !!object.message)
)

export const intersectConnectionClick$ = intersectClick$.pipe(
  filter((object) => object.connection && object.connection.destinationId)
)

export const started$ = start$.pipe(filter((s) => !!s))
