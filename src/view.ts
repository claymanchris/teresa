import * as THREE from 'three'
import { getMessages$, getVideoUrl$ } from './firebase'
import { resize$, pan$, started$ } from './observables'
import data from './data.json'
import _ from 'lodash'

import {
  latLng,
  distance,
  currentRoom,
  renderer,
  camera,
  scene,
  sphereGroup,
  messageGroup,
  currentVideo
} from './subjects'

// let hasInteracted = false
//
//

currentRoom.subscribe((room) => {
  if (room) {
    clearMessagesAndConnectionsFromScene()

    getVideoUrl$(room.video).subscribe((url) => {
      if (url) {
        playVideo(url)
      }
    })

    paintConnections(room.connections)

    // const messagesRef = room.ref.collection('messages')
    // getMessages$(messagesRef).subscribe({
    // next(messages) {
    // paintMessages(messages)
    // }
    // })
  }
})

export const paintMessage = (message) => {
  const loader = new THREE.FontLoader()
  loader.load('./fonts/texturina.json', (font) => {
    const color = 0x006699

    const matLite = new THREE.MeshBasicMaterial({
      color: color,
      transparent: true,
      opacity: 0.4,
      side: THREE.DoubleSide
    })

    const shapes = font.generateShapes(message.text, 100)

    const geometry = new THREE.ShapeBufferGeometry(shapes)

    geometry.computeBoundingBox()

    const xMid =
      -0.5 * (geometry.boundingBox.max.x - geometry.boundingBox.min.x)

    geometry.translate(xMid, 0, 0)

    // make shape ( N.B. edge view not visible )
    const text = new THREE.Mesh(geometry, matLite)
    text.position.z = -300
    scene.add(text)
  })
}

const paintConnections = (connections) => {
  // sphere
  const sphereRadius = 30
  const sphereWidthDivisions = 32
  const sphereHeightDivisions = 16
  const sphereGeometry = new THREE.SphereBufferGeometry(
    sphereRadius,
    sphereWidthDivisions,
    sphereHeightDivisions
  )
  const sphereMaterial = new THREE.MeshPhongMaterial({ color: '#cd300f' })

  connections.forEach((connection) => {
    const connectionMesh = new THREE.Mesh(sphereGeometry, sphereMaterial)
    connectionMesh.position.set(...connection.position)
    connectionMesh.connection = connection
    sphereGroup.add(connectionMesh)
  })
  scene.add(sphereGroup)
}

const paintMessages = (messages) => {
  const sphereRadius = 15
  const sphereWidthDivisions = 32
  const sphereHeightDivisions = 16
  const sphereGeometry = new THREE.SphereBufferGeometry(
    sphereRadius,
    sphereWidthDivisions,
    sphereHeightDivisions
  )

  messages.forEach((message) => {
    const basicMaterial = new THREE.MeshPhongMaterial({
      color: message.color || '#8Ac'
    })
    const messageDot = new THREE.Mesh(sphereGeometry, basicMaterial)
    messageDot.position.set(...message.position)
    messageDot.message = message
    messageGroup.add(messageDot)
  })
  scene.add(messageGroup)
}

const clearMessagesAndConnectionsFromScene = () => {
  while (sphereGroup.children.length) {
    sphereGroup.remove(sphereGroup.children[0])
  }
  while (messageGroup.children.length) {
    messageGroup.remove(messageGroup.children[0])
  }
}

const resizeCanvasToDisplaySize = () => {
  renderer.setSize(window.innerWidth, window.innerHeight)
  camera.aspect = window.innerWidth / window.innerHeight
  camera.updateProjectionMatrix()
}

resize$.subscribe(resizeCanvasToDisplaySize)

const animate = () => {
  requestAnimationFrame(animate)
  update()
}

pan$.subscribe(() => {
  // hasInteracted = true
})

const update = () => {
  const newLat = Math.max(-85, Math.min(85, latLng.value[0]))
  let newLng = latLng.value[1]
  // wrap around. TODO: make cleaner
  if (newLng >= 180) {
    const diff = newLng - 180
    newLng = -180 + diff
  } else if (newLng <= -180) {
    const diff = newLng + 180
    newLng = 180 + diff
  }
  latLng.next([newLat, newLng])
  renderer.render(scene, camera)
}

const removeVideo = (videoObject) => {
  const stretchAndFadeOut = () => {
    videoObject.material.opacity -= 0.05
    if (videoObject.material.opacity > 0) {
      window.requestAnimationFrame(stretchAndFadeOut)
    } else {
      scene.remove(videoObject)
      const video = document.getElementById('video')
      if (video) {
        video.remove()
      }
    }
  }

  window.requestAnimationFrame(stretchAndFadeOut)
}

const playVideo = (videoUrl) => {
  if (currentVideo.value) {
    removeVideo(currentVideo.value)
  }

  // video code should be derived from shared 'map', connected to sphere directions

  const { mesh } = createVideo(videoUrl)
  currentVideo.next(mesh)
}

// creates video and fades in animation
const createVideo = (src) => {
  const videoEl = document.createElement('video')
  videoEl.id = 'video'
  videoEl.setAttribute('preload', 'auto')
  videoEl.setAttribute('muted', 'true')
  videoEl.setAttribute('loop', 'true')
  videoEl.setAttribute('playsinline', 'true')
  videoEl.setAttribute('crossorigin', 'anonymous')
  videoEl.setAttribute('style', 'display: none')

  const source = document.createElement('source')
  source.src = src

  videoEl.appendChild(source)
  document.body.appendChild(videoEl)

  const texture = new THREE.VideoTexture(videoEl)
  // texture.minFilter = THREE.LinearFilter
  // texture.magFilter = THREE.LinearFilter
  // texture.format = THREE.RGBFormat
  texture.anisotropy = renderer.capabilities.getMaxAnisotropy()

  const fadeIn = () => {
    videoMaterial.opacity += 0.05
    if (videoMaterial.opacity < 1) {
      window.requestAnimationFrame(fadeIn)
    } else {
      // hasInteracted = false
    }
  }

  videoEl.addEventListener('play', () => {
    window.requestAnimationFrame(fadeIn)
  })

  const videoMaterial = new THREE.MeshBasicMaterial({ map: texture })
  videoMaterial.transparent = true
  videoMaterial.opacity = 0
  let mesh = new THREE.Mesh(geometry, videoMaterial)
  scene.add(mesh)

  videoEl.load()
  videoEl.play()
  return { mesh, videoEl }
}

latLng.subscribe((val) => {
  const phi = THREE.MathUtils.degToRad(90 - val[0])
  const theta = THREE.MathUtils.degToRad(val[1])

  const x = distance.value * Math.sin(phi) * Math.cos(theta)
  const y = distance.value * Math.cos(phi)
  const z = distance.value * Math.sin(phi) * Math.sin(theta)

  camera.position.x = x
  camera.position.y = y
  camera.position.z = z

  if (camera && camera.target) {
    camera.lookAt(camera.target)
  }
})

// initial setup
const container = document.getElementById('container')
camera.target = new THREE.Vector3(0, 0, 0)

// background and lights
scene.background = new THREE.Color('#000')
const light = new THREE.DirectionalLight(0xffffff, 0.2)
light.position.set(0, 10, 0)
light.target.position.set(-5, 0, 0)
scene.add(light)
scene.add(light.target)
const light2 = new THREE.DirectionalLight(0xffffff, 1)
light2.position.set(0, -10, 0)
light2.target.position.set(5, 0, 0)
scene.add(light2)
scene.add(light2.target)

// video texture
const geometry = new THREE.SphereBufferGeometry(500, 60, 40)
// invert the geometry on the x-axis so that all of the faces point inward
geometry.scale(-1, 1, 1)

// arrow axis helpers
const xGeometry = new THREE.BufferGeometry().setFromPoints([
  new THREE.Vector3(-1000, 0, 0),
  new THREE.Vector3(1000, 0, 0)
])
const xMaterial = new THREE.LineBasicMaterial({ color: 0x0000ff })
const xLine = new THREE.Line(xGeometry, xMaterial)

scene.add(xLine)

const yGeometry = new THREE.BufferGeometry().setFromPoints([
  new THREE.Vector3(0, -1000, 0),
  new THREE.Vector3(0, 1000, 0)
])
const yMaterial = new THREE.LineBasicMaterial({ color: 0x00ff00 })
const yLine = new THREE.Line(yGeometry, yMaterial)

scene.add(yLine)

const zGeometry = new THREE.BufferGeometry().setFromPoints([
  new THREE.Vector3(0, 0, -1000),
  new THREE.Vector3(0, 0, 1000)
])
const zMaterial = new THREE.LineBasicMaterial({ color: 0xff0000 })
const zLine = new THREE.Line(zGeometry, zMaterial)

scene.add(zLine)

// set up renderer and start animation
renderer.setPixelRatio(window.devicePixelRatio)
renderer.setSize(window.innerWidth, window.innerHeight)
renderer.outputEncoding = THREE.sRGBEncoding
container.appendChild(renderer.domElement)

started$.subscribe(animate)
