import '@babel/polyfill'
import Vue from 'vue'
import * as Rx from 'rxjs'
import VueRx from 'vue-rx'
import Buefy from 'buefy'
import data from './data.json'
import 'buefy/dist/buefy.css'
import App from './App'
import { paintMessage } from './view'
import { db, getRoom$, login } from './firebase'
import {
  pan$,
  wheel$,
  intersectEnter$,
  intersectLeave$,
  intersectConnectionClick$,
  intersectMessageClick$,
  started$
} from './observables'
import { latLng, distance, currentRoom, user } from './subjects'

const START_ROOM_ID = '6020e9a3-0ac5-4161-82c5-c639d2560425'

Vue.use(VueRx, Rx)
Vue.use(Buefy)

login().then((u) => user.next(u))

new Vue({
  el: '#app',
  render: (h) => h(App)
})

pan$.subscribe(({ down, move, originalLng, originalLat }) => {
  latLng.next([
    (down.clientY - move.clientY) * 0.1 + originalLat,
    (down.clientX - move.clientX) * 0.1 + originalLng
  ])
})

wheel$.subscribe((d) => distance.next(d))

intersectEnter$.subscribe((object) => {
  console.log('mouse enter', object)
  document.body.style.cursor = 'pointer'
})

intersectLeave$.subscribe((object) => {
  console.log('mouse leave', object)
  document.body.style.cursor = 'default'
})

intersectMessageClick$.subscribe((object) => {
  paintMessage(object.message)
})

intersectConnectionClick$.subscribe((object) => {
  console.log('mouse click', object)

  changeRoom(data.rooms[object.connection.destinationId])
})

// const changeRoom = (roomRef) => getRoom$(roomRef).subscribe(currentRoom)
const changeRoom = (room) => currentRoom.next(room)

started$.subscribe(() => {
  changeRoom(data.rooms[START_ROOM_ID])
  // changeRoom(db.collection('rooms').doc('TqN7uGgk8Fmmvz46npnW'))
})
