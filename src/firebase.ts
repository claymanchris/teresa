import firebase from 'firebase'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/storage'
import { doc, collection } from 'rxfire/firestore'
import { getDownloadURL } from 'rxfire/storage'
import { filter, map, switchMap } from 'rxjs/operators'

const firebaseConfig = {
  apiKey: 'AIzaSyAMKuP3Tx9zDnPF_ROuvFdj3_eAHPeGv5Q',
  authDomain: 'teresa-b90a8.firebaseapp.com',
  databaseURL: 'https://teresa-b90a8.firebaseio.com',
  projectId: 'teresa-b90a8',
  storageBucket: 'teresa-b90a8.appspot.com',
  messagingSenderId: '502861345286',
  appId: '1:502861345286:web:5e04677a15d85461778987',
  measurementId: 'G-10J27MGNTK'
}

// Initialize Firebase
export const app = firebase.initializeApp(firebaseConfig)
app.analytics()

export const db = app.firestore()
const storage = app.storage()

export const addMessage = (message: any) => {
  console.log('TODO: Implement', message)
  return Promise.resolve()
  // return message.roomRef.collection('messages').add({
  //   text: message.text,
  //   position: message.position,
  //   createdAt: new Date(),
  //   locale: 'en'
  // })
}

export const getVideoUrl$ = (videoTitle) => {
  const storageRef = storage.ref()
  const videoRef = storageRef.child(`videos/${videoTitle}.webm`)
  return getDownloadURL(videoRef)
}

export const getRoom$ = (roomRef) => {
  return doc(roomRef).pipe(
    map((room) => room.data()),
    filter((room) => !!room),
    switchMap((room) => {
      return getVideoUrl$(room.videoTitle).pipe(
        map((videoUrl) => {
          return { videoUrl, ...room }
        })
      )
    }),
    switchMap((room) => {
      const connectionsRef = roomRef.collection('connections')
      return collection(connectionsRef).pipe(
        map((connections) => ({
          ...room,
          ref: roomRef,
          connections: connections.map((d) => d.data())
        }))
      )
    })
  )
}

export const getMessages$ = (messagesRef) => {
  return collection(messagesRef).pipe(
    map((messages) => messages.map((d) => d.data()))
  )
}

export const login = () => app.auth().signInAnonymously()
