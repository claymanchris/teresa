import { BehaviorSubject } from 'rxjs'
import * as THREE from 'three'
export const latLng = new BehaviorSubject<number[]>([0, 0])
export const distance = new BehaviorSubject<number>(50)
export const currentRoom = new BehaviorSubject<any>(null)
export const user = new BehaviorSubject<any>(null)
export const renderer = new THREE.WebGLRenderer({ antialias: true })
export const camera = new THREE.PerspectiveCamera(70, 2, 1, 1000)
export const scene = new THREE.Scene()
export const sphereGroup = new THREE.Group()
export const messageGroup = new THREE.Group()
export const formOpen = new BehaviorSubject<boolean>(false)
export const currentVideo = new BehaviorSubject<any>(null)
export const start$ = new BehaviorSubject<boolean>(false)
